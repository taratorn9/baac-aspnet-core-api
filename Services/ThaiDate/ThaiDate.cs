﻿using System.Globalization;

namespace BAACAspNetCoreAPI.Services.ThaiDate
{
    public class ThaiDate : IThaiDate
    {
        public string ShowThaiDate()
        {
            return DateTime.Now.ToString("dd MMMM yyyy", new CultureInfo("th-TH"));
        }
    }
}
