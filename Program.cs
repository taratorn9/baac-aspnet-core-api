using BAACAspNetCoreAPI.Areas.Identity.Data;
using BAACAspNetCoreAPI.Data;
using BAACAspNetCoreAPI.Services.ThaiDate;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

// Add CORS
builder.Services.AddCors();

// Add services to the container.
builder.Services.AddDbContext<APIContext>(options =>
{
 options.UseSqlServer(builder.Configuration.GetConnectionString("BAACAPICONTEXT")
     ?? throw new InvalidOperationException("Connectionstring not found"));
});

builder.Services.AddDbContext<IdentityContext>(options =>
{
 options.UseSqlServer(builder.Configuration.GetConnectionString("IdentityContext")
     ?? throw new InvalidOperationException("Connectionstring not found"));
});
builder.Services.AddDefaultIdentity<BaacUser>(options => options.SignIn.RequireConfirmedAccount = false).AddEntityFrameworkStores<IdentityContext>();

builder.Services.Configure<IdentityOptions>(options =>
{
 options.Password.RequiredLength = 3;
 options.Password.RequireNonAlphanumeric = false;
 options.Password.RequireDigit = false;
 options.Password.RequireUppercase = false;
 options.Password.RequireLowercase = false;
 options.SignIn.RequireConfirmedEmail = false;

});
// add jwt service for validate token
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
{
 options.TokenValidationParameters = new TokenValidationParameters()
 {
  ValidateIssuerSigningKey = true,
  IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration.GetSection("JWT_KEY").Value!)),
  ValidateIssuer = false,
  ValidateAudience = false,
  ClockSkew = TimeSpan.Zero
 };
});

builder.Services.AddControllers().AddJsonOptions(options =>
{
 options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
 options.JsonSerializerOptions.MaxDepth = 64;
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(opt =>
{
 opt.SwaggerDoc("v1", new OpenApiInfo { Title = "MyAPI", Version = "v1" });
 opt.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
 {
  In = ParameterLocation.Header,
  Description = "Please enter token",
  Name = "Authorization",
  Type = SecuritySchemeType.Http,
  BearerFormat = "JWT",
  Scheme = "bearer"
 });

 opt.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type=ReferenceType.SecurityScheme,
                    Id="Bearer"
                }
            },
            new string[]{}
        }
    });
});

// Custom services
builder.Services.AddScoped<IThaiDate, ThaiDate>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
 app.UseSwagger();
 app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(options =>
{
 options.AllowAnyOrigin();
 options.AllowAnyMethod();
 options.AllowAnyHeader();
 // options.WithOrigins("https://app.baac.com").AllowAnyMethod();
});

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
