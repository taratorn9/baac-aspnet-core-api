﻿using BAACAspNetCoreAPI.Models;
using Microsoft.EntityFrameworkCore;


namespace BAACAspNetCoreAPI.Data
{
    public class APIContext : DbContext
    {
        public APIContext(DbContextOptions<APIContext> options)
            : base(options)
        {
              
        }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Blog> Blogs { get; set; }
    }
}
