﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BAACAspNetCoreAPI.Data;
using BAACAspNetCoreAPI.Models;

namespace BAACAspNetCoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly APIContext _context;

        public CategoryController(APIContext context)
        {
            _context = context;
        }

        // GET: api/Category
        [HttpGet]
        public async Task<IActionResult> GetCategories()
        {
            var category = await _context.Categories
                .Select(c => new { c.Id, c.Name, c.IsActive })
                .AsNoTracking()
                .ToListAsync();

            var totalRecord = await _context.Categories.CountAsync();

            return Ok(new {total = totalRecord,
            data = category});
        }
        // Pagination
        // GET: api/Category/pagination?page=1&pageSize=3
        [HttpGet("pagination")]
        public async Task<IActionResult> GetCategoryWithPagination([FromQuery] int page = 1,[FromQuery] int pageSize = 3)
        {
            var category = await _context.Categories
                .Select(c => new { c.Id, c.Name, c.IsActive })
                .OrderByDescending(c => c.Id)
                .Skip((page-1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();

            var totalRecord = await _context.Categories.CountAsync();

            return Ok(new
            {
                total = totalRecord,
                data = category
            });
        }
        // Search by category name
        // GET: api/Category/search?name=sport
        [HttpGet("search")]
        public async Task<IActionResult> SearchCategory([FromQuery] string name)
        {
            var category = await _context.Categories
                .Select(c => new { c.Id, c.Name, c.IsActive })
                .Where(c => EF.Functions.Like(c.Name, $"%{name}%"))
                .OrderByDescending(c => c.Id)
                .AsNoTracking()
                .ToListAsync();

            return Ok(category);
        }
        // GET: api/Category/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Category>> GetCategory(int id)
        {
            //var category = await _context.Categories.FindAsync(id);
            var category = await _context.Categories
                .Include(c => c.Blogs)
                .Where(c => c.Id == id)
                .SingleOrDefaultAsync();

            if (category == null)
            {
                return NotFound();
            }

            return category;
        }

        // PUT: api/Category/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategory(int id, Category category)
        {
            if (id != category.Id)
            {
                return BadRequest();
            }

            _context.Entry(category).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Category
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Category>> PostCategory(Category category)
        {
            await _context.Categories.AddAsync(category);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCategory", new { id = category.Id }, new
            {
                message = "เพิ่มข้อมูลสำเร็จ",
                data = category
            });
        }

        // DELETE: api/Category/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            var category = await _context.Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound(new {message = $"Data id:{id} not found"});
            }

            _context.Categories.Remove(category);
            await _context.SaveChangesAsync();

            return Ok(new {message = $"Data id:{id} Deleted"});
        }

        private bool CategoryExists(int id)
        {
            return _context.Categories.Any(e => e.Id == id);
        }
    }
}
