﻿using BAACAspNetCoreAPI.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BAACAspNetCoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly APIContext _context;
        public BlogController(APIContext context)
        {
            _context = context;
        }
        [Authorize] // Need jwt to access this
        [HttpGet("")]
        public async Task<IActionResult> Index()
        {
            var blog = await _context.Blogs
                .Include(c => c.Category)
                .Select(b => new { b.Id, b.Topic, CategoryName = new { b.Category.Id, b.Category.Name, b.Category.Description } })
                .AsNoTracking()
                .ToListAsync();

            return Ok(blog);
        }
    }
}
