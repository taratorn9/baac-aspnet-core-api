﻿using BAACAspNetCoreAPI.ModelsDto;
using Microsoft.AspNetCore.Mvc;

namespace BAACAspNetCoreAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        [Route("")]
        [HttpGet]
        public IActionResult Index()
        {
            return Ok(new { message = "Company Index" });
        }
        [HttpGet("{id}/name/{name}")]
        public IActionResult GetCompanyById([FromRoute] int id, [FromRoute] string name) 
        {
            return Ok(new { message = $"ID: {id} Name: {name}" });
        }
        [HttpGet("search")]
        public IActionResult SearchCompany([FromQuery] string name, [FromQuery] string sort)
        {
            return Ok(new { message = $"{name} {sort}" });
        }
        [HttpPost("")]
        public IActionResult CreateCompany([FromBody] CompanyDto companyDto)
        {
            return Ok(new { message = $"{companyDto.Name} {companyDto.Address}" });
        }
        [HttpPost("create")]
        public IActionResult CreateCompany2([FromQuery] CompanyDto companyDto)
        {
            return Ok(new { message = $"{companyDto.Name} {companyDto.Address}" });
        }
    }
}
