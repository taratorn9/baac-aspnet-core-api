﻿using BAACAspNetCoreAPI.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BAACAspNetCoreAPI.Controllers
{
    [Route("api/v1")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly APIContext _context;

        public HomeController(APIContext context)
        {
            _context = context;
        }
        [HttpGet("")]
        public IActionResult Index()
        {
            return Ok(new { message = "BAAC API v1 Running..." });
        }
        [HttpGet("about")]
        public IActionResult About()
        {
            return Ok(new { message = "Hello About Us" });
        }
        [HttpGet("customer")]
        public async Task<IActionResult> GetCustomer()
        {
            var customer = await _context.Customer.FromSqlRaw("select * from customer").ToListAsync();
                return Ok(customer);
        }
    }
}
