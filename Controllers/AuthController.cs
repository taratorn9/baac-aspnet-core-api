﻿using BAACAspNetCoreAPI.Areas.Identity.Data;
using BAACAspNetCoreAPI.ModelsDto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace BAACAspNetCoreAPI.Controllers
{
 [Route("api/[controller]/[action]")]
 [ApiController]
 public class AuthController : ControllerBase
 {
  private readonly UserManager<BaacUser> _userManager;
  private readonly SignInManager<BaacUser> _signInManager;
  private readonly IConfiguration _configuration;

  public AuthController(UserManager<BaacUser> userManager, SignInManager<BaacUser> signInManager, IConfiguration configuration)
  {
   _userManager = userManager;
   _signInManager = signInManager;
   _configuration = configuration;
  }
  [HttpPost("register")]
  public async Task<IActionResult> Register([FromBody] RegisterDto registerDto)
  {
   var user = await _userManager.FindByEmailAsync(registerDto.Email);
   if (user != null) return Conflict(new { message = "อีเมล์ซ้ำ" });

   var baacUser = new BaacUser
   {
    Firstname = registerDto.FirstName,
    Lastname = registerDto.LastName,
    Email = registerDto.Email,
    UserName = registerDto.Email
   };
   var result = await _userManager.CreateAsync(baacUser, registerDto.Password);
   if (!result.Succeeded)
   {
    return BadRequest(result.Errors);
   }

   return Created("", new { message = "register" });
  }

  [HttpPost("login")]
  public async Task<IActionResult> Login([FromBody] LoginDto loginDto)
  {
   var user = await _userManager.FindByEmailAsync(loginDto.Email);
   if (user == null) return Unauthorized(new { message = "ไม่พบผู้ใช้นี้ในระบบ" });

   var result = await _signInManager.PasswordSignInAsync(user, loginDto.Password, false, false);
   if (!result.Succeeded)
   {
    return Unauthorized(new { message = "รหัสผ่านไม่ถูกต้อง" });
   }
   return await CreateToken(user.Email!);
  }

  // create JWT token
  private async Task<IActionResult> CreateToken(string email)
  {
   var user = await _userManager.FindByEmailAsync(email);
   if (user == null) return NotFound();

   // เตรียม Payload สำหรัลสร้าง token
   var payload = new List<Claim>
            {
                new("userId", user.Id),
                new(JwtRegisteredClaimNames.Email, user.Email!)
            };

   // เพิ่ม role ของ user แต่ละคนเข้าไปใน payload ด้วย (role-base authentication)
   //var userRoles = await _userManager.GetRolesAsync(user);
   //if (userRoles != null)
   //{
   // payload.AddRange(userRoles.Select(role => new Claim(ClaimsIdentity.DefaultRoleClaimType, role)));
   //}

   // ดึง JWT_KEY จาก appsettings.json
   var jwtKey = Encoding.UTF8.GetBytes(_configuration.GetSection("JWT_KEY").Value!);

   // เตรียมข้อมูล payload token / Algorithms / expire 
   var tokenDescriptor = new SecurityTokenDescriptor
   {
    Subject = new ClaimsIdentity(payload),
    SigningCredentials = new SigningCredentials(
           new SymmetricSecurityKey(jwtKey), SecurityAlgorithms.HmacSha256),
    Expires = DateTime.UtcNow.AddDays(7)
   };

   // สร้าง token
   var tokenHandler = new JwtSecurityTokenHandler();
   var token = tokenHandler.CreateToken(tokenDescriptor);

   return Ok(new
   {
    access_token = tokenHandler.WriteToken(token),
    expiration = token.ValidTo
   });
  }
  // Get user's profile
  [Authorize]
  [HttpGet] // /api/auth/profile
  public async Task<IActionResult> Profile()
  {
   // Get userId from payload token
   var userId = User.Claims.First(p => p.Type == "userId").Value;

   // Get user profile from db
   var userProfile = await _userManager.FindByIdAsync(userId);
   if(userProfile == null) return NotFound();

   return Ok(new { userProfile.Id, userProfile.Email, userProfile.Firstname, userProfile.Lastname });
  }
 }
}
