using BAACAspNetCoreAPI.Services.ThaiDate;
using Microsoft.AspNetCore.Mvc;

namespace BAACAspNetCoreAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IThaiDate _thaiDate;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IThaiDate thaiDate)
        {
            _logger = logger;
            _thaiDate = thaiDate;
        }

        [HttpGet("getWeatherForecast")]
        public IEnumerable<WeatherForecast> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpGet("getThaiDate")]
        public IActionResult GetThaiDate()
        {
            return Ok(new { message = $"�ѹ����ѹ��� {_thaiDate.ShowThaiDate()}" });
        }
    }
}
