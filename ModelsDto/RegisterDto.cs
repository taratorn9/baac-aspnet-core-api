﻿using System.ComponentModel.DataAnnotations;

namespace BAACAspNetCoreAPI.ModelsDto
{
 public class RegisterDto
 {
  [Required(ErrorMessage = "ชื่อห้ามว่าง")]
  public string FirstName { get; set; } = null!;
  [Required(ErrorMessage = "นามสกุลห้ามว่าง")]
  public string LastName { get; set; } = null!;
  [Required(ErrorMessage = "emailห้ามว่าง")]
  [EmailAddress(ErrorMessage = "รูปแบบ mail ไม่ถูกต้อง")]
  public string Email { get; set; } = null!;
  [Required(ErrorMessage = "ชื่อห้ามว่าง")]
  [StringLength(100, ErrorMessage = "รหัสอย่างน้อย {2} ไม่เกิน {1} ตัวอักษร", MinimumLength = 3)] 
  public string Password { get; set; } = null!;

 }
}
