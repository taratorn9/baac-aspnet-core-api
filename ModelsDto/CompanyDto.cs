﻿using System.ComponentModel.DataAnnotations;

namespace BAACAspNetCoreAPI.ModelsDto
{
    public class CompanyDto
    {
        [Required(ErrorMessage = "ป้อนชื่อบริษัทด้วย")]
        public string Name { get; set; } = null!;
        public string Address { get; set; } = null!;
    }
}
