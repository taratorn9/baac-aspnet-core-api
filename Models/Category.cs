﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BAACAspNetCoreAPI.Models
{
    [Table("category")]
    public class Category
    {
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        public string Name { get; set; } = null!;
        [Column("isactive")]
        public bool IsActive { get; set; }
        [Column("description", TypeName = "text")]
        public string Description { get; set; } = null!;

        // Relation
        // One to Many
        public ICollection<Blog>? Blogs { get; set;}
    }
}
