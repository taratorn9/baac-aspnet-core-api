﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BAACAspNetCoreAPI.Models
{
    [Table("blog")]
    public class Blog
    {
        [Column("id")]
        public int Id { get; set; }
        [Column("topic")]
        public string Topic { get; set; } = null!;

        // Relation
        // Many to one
        //[ForeignKey("id")]
        public Category Category { get; set; } = null!;
    }
}
