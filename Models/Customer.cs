﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BAACAspNetCoreAPI.Models
{
    [NotMapped]
    [Table("customer")]
    public class Customer
    {
        [Key] // Primary key annotation, don't have to if named "Id"
        [Column("id")]
        public int Id { get; set; }

        [Column("name", TypeName = "nvarchar")]
        public string Name { get; set; } = null!;
    }
}
