﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace BAACAspNetCoreAPI.Areas.Identity.Data;

// Add profile data for application users by adding properties to the  class
public class BaacUser: IdentityUser
{
    // Custom column
    public string Firstname { get; set; } = null!;
    public string Lastname { get; set;} = null!;
}

